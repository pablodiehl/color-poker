# Color Poker
A simple planning poker tool that runs right from your browser.

**Made with:**
- [Mithril.js](https://mithril.js.org/)
- [Stylus](http://stylus-lang.com/)