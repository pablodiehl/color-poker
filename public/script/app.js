var selectedCard = null,
    cardColor = null;

var renderCardList = {
    view: function() {
        var list =  ['0', '½', '1', '2', '3', '5', '8', '13', '21', '34', '55', '100', '?', '∞', '☕'];
        return m('div',
            [
                m('div',
                    { class: 'main'},
                    list.map(function(item, index) {
                        var color = Math.floor(index/3);
                        return m('div',
                            {
                                class: 'card-link background-' + 
                                        color,
                                color:  color,
                                number: item,
                                onclick: function(){
                                    selectedCard = this.getAttribute('number');
                                    cardColor = this.getAttribute('color');
                                    m.mount(document.body, cardComponent);
                                }
                            },
                            [m('h1', item)]
                        );
                   })
                ),
                m('div',
                    { class: 'credits'},
                    [   
                        m('h1', 'Made with <3 by Pablo Diehl'),
                        m('h1',
                            m('a', 
                            {
                              href: 'https://gitlab.com/pablodiehl/color-poker',
                              target: '_blank'
                            },
                            'Get the source code!'),
                        ),
                        m('h1',
                            m('a', 
                            {
                              href: "https://twitter.com/intent/tweet?text=It's%20simple,%20it's%20colorful,%20it's%20for%20the%20web!%20%23ColorPokerRocks%0A",
                              target: '_blank'
                            },
                            'Love it? Share on Twitter!'),
                        )
                    ]
                )
            ]
        )
    }
};

var cardComponent = {
    view: function(w) {
        return m('div',
            {
                class: 'card background-' + cardColor,
                onclick: function(){
                    m.mount(document.body, renderCardList);
                }
            },
            [m('h1', selectedCard)]
        );
    }
};

m.mount(document.body, renderCardList);
